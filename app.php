<?php

declare(strict_types=1);

require_once('App/ParseXml.php');
require_once('App/Visualize.php');
require_once('App/Species.php');
require_once('App/GameOfLife.php');

$xml = new \App\ParseXml('Xml/world.xml');
$species = new \App\Species($xml->getDimension(), $xml->getSpecies());
$game = new \App\GameOfLife($xml->getIteration(), $species);

try {
    $xml->validateXml();
    $game->run(true);
} catch (\Exception | \Throwable $e) {
    echo $e->getMessage();
}