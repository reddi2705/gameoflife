<?php

declare(strict_types = 1);

namespace App;

class Visualize
{
    /**
     * @return void
     */
    public static function header(): void
    {
        echo '<div style="display:inline-block; border:1px solid black; margin:0 10px 10px 0;">';
    }

    /**
     * @return void
     */
    public static function footer(): void
    {
        echo '</div>';
    }

    /**
     * @return void
     */
    public static function breakLine(): void
    {
        echo '<div style="clear:both;"></div>';
    }

    /**
     * @return void
     */
    public static function emptyCell(): void
    {
        echo '<div style="float:left; width:25px; height:25px; border:1px solid black;"></div>';
    }

    /**
     * @param string $organism
     * @return void
     */
    public static function fullCell(string $organism): void
    {
        echo '<div style="float:left; background-color:red; width:25px; height:25px; border:1px solid black; text-align:center; line-height:25px;">'
            . strtoupper($organism)
            . '</div>';
    }
}