<?php

declare(strict_types=1);

namespace App;

/**
 * Class GameOfLife
 * @package App
 */
class GameOfLife
{
    /** @var Species */
    private $species;
    /** @var int */
    private $iteration;

    /**
     * @param int $iteration
     * @param Species $species
     */
    public function __construct(int $iteration, Species $species)
    {
        $this->iteration = $iteration;
        $this->species = $species;
        $this->species->init();
    }

    /**
     * @param bool $visualise
     * @return void
     */
    public function run(bool $visualise = false): void
    {
        for ($i = 0; $i < $this->iteration; ++$i) {
            if ($this->species->emptyWorld() === $this->species->getActualIteration() // stop if everything is dead
                || $this->species->getActualIteration() === $this->species->getPreviousIteration()) // stop if stable state
            {
                break;
            }

            if ($visualise) {
                $this->species->wrapWorld($this->species->getActualIteration());
            }

            $this->species->nextIteration($this->species->getActualIteration());
        }

        $this->species->saveWorld($this->species->getPreviousIteration()); // save XML
    }
}