<?php

declare(strict_types=1);

namespace App;

/**
 * Class ParseXml
 * @package App
 */
class ParseXml
{
    /** @var \SimpleXMLElement */
    private $xml;

    /**
     * @param string $xml
     */
    public function __construct(string $xml)
    {
        $this->xml = @simplexml_load_file($xml);
    }

    /**
     * @return int
     */
    public function getDimension(): int
    {
        return (int)$this->xml->world->cells;
    }

    /**
     * @return int
     */
    public function getIteration(): int
    {
        return (int)$this->xml->world->iterations;
    }

    /**
     * @return int
     */
    public function getCountSpecies(): int
    {
        return (int)$this->xml->world->species;
    }

    /**
     * @return \Generator
     */
    public function getSpecies(): \Generator
    {
        foreach ($this->xml->organisms->organism as $cell) {
            yield $cell;
        }
    }

    /**
     * @param mixed[] $lastIteration
     * @return \SimpleXMLElement
     */
    public function generateXml(array $lastIteration): \SimpleXMLElement
    {
        $dimension = $this->getDimension();

        $xml = new \SimpleXMLElement('<life/>');
        $world = $xml->addChild('world');
        $world->addChild('cells', (string) $dimension);
        $world->addChild('species', (string) $this->getCountSpecies());
        $world->addChild('iterations', (string) $this->getIteration());

        $organisms = $xml->addChild('organisms');

        for ($x = 0; $x < $dimension; ++$x) {
            for ($y = 0; $y < $dimension; ++$y) {
                if($lastIteration[$x][$y] !== null) {
                    $organism = $organisms->addChild('organism');
                    $organism->addChild('x_pos', (string) $x);
                    $organism->addChild('y_pos', (string) $y);
                    $organism->addChild('species', $lastIteration[$x][$y]);
                }
            }
        }

        return $xml;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function validateXml(): bool
    {
        libxml_use_internal_errors(true);

        if ($this->xml === false) {
            throw new \Exception('Invalid XML.');
        } else {
            $countSpecies = $this->getCountSpecies();
            $species = [];
            $counter = 0;

            foreach ($this->getSpecies() as $cell) {
                $species[] = (string)$cell->species;
                $counter++;
            }

            if ($countSpecies !== count(array_unique($species))) {
                throw new \Exception('Wrong count species.');
            } elseif ($counter > pow($this->getDimension(), 2)) {
                throw new \Exception('Too many organisms.');
            }
        }
        return true;
    }
}