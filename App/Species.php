<?php

declare(strict_types = 1);

namespace App;

/**
 * Class Species
 * @package App
 */
class Species
{
    /** @var int */
    private $dimension;
    /** @var array */
    private $nextIteration;
    /**  @var array */
    private $actualIteration;
    /** @var \Generator */
    private $organisms;

    /**
     * Species constructor.
     *
     * @param int $dimension
     * @param \Generator $organisms
     */
    public function __construct(int $dimension, \Generator $organisms)
    {
        $this->dimension = $dimension;
        $this->organisms = $organisms;
    }

    /**
     * @return array
     */
    public function init(): array
    {
        $this->nextIteration = $this->emptyWorld();

        foreach ($this->organisms as $cell) {
            $this->nextIteration[(int)$cell->x_pos][(int)$cell->y_pos] = (string)$cell->species;
        }

        return $this->nextIteration;
    }

    /**
     * @return array
     */
    public function emptyWorld(): array
    {
        $emptyWorld = [];

        for ($x = 0; $x < $this->dimension; ++$x) {
            for ($y = 0; $y < $this->dimension; ++$y) {
                $emptyWorld[$x][$y] = null;
            }
        }

        return $emptyWorld;
    }

    /**
     * @param mixed[] $actualIteration
     * @return array
     */
    public function nextIteration(array $actualIteration): array
    {
        $this->nextIteration = [];
        $this->setPreviousIteration($actualIteration);

        for ($x = 0; $x < $this->dimension; $x++) {
            for ($y = 0; $y < $this->dimension; $y++) {
                $neighbour = $this->findNeighbours($this->actualIteration, $x, $y);

                foreach ($neighbour as $organism => $count) {
                    if ((($this->actualIteration[$x][$y] !== $organism) && ($this->nextIteration[$x][$y] === null) && ($count == 3)) // reborn
                        || (($this->actualIteration[$x][$y] === $organism) && (($count == 2) || ($count == 3))) // still alive
                    ) {
                        $this->nextIteration[$x][$y] = $organism;
                    } else if (($this->actualIteration[$x][$y] !== $organism) && $count == 3) { // collision random
                        $this->nextIteration[$x][$y] = array_rand($neighbour);
                    }
                }
            }
        }

        return $this->nextIteration;
    }

    /**
     * @param mixed[] $actualIteration
     * @param int $x
     * @param int $y
     * @return array
     */
    private function findNeighbours(array $actualIteration, int $x, int $y): array
    {
        $neighbours = [];

        for ($rowMod = -1; $rowMod <= 1; $rowMod++) {
            for ($colMod = -1; $colMod <= 1; $colMod++) {
                if ($x + $rowMod < 0
                    || $x + $rowMod >= $this->dimension
                    || $y + $colMod < 0
                    || $y + $colMod >= $this->dimension
                    || ($rowMod == 0 && $colMod == 0)
                ) {
                    continue; // out of boundary
                } else {
                    if ($actualIteration[$x + $rowMod][$y + $colMod] !== null) {
                        $neighbours[$actualIteration[$x + $rowMod][$y + $colMod]] += isset($actualIteration[$x + $rowMod][$y + $colMod]) ?: 0;
                    }
                }
            }
        }

        return $neighbours;
    }

    /**
     * @return array
     */
    public function getActualIteration(): array
    {
        return $this->nextIteration;
    }

    /**
     * @param mixed[] $actualIteration
     * @return array
     */
    private function setPreviousIteration(array $actualIteration): array
    {
        $this->actualIteration = $actualIteration;
        return $this->actualIteration;
    }

    /**
     * @return array|null
     */
    public function getPreviousIteration(): ?array
    {
        return $this->actualIteration;
    }

    /**
     * @param mixed[] $lastIteration
     */
    public function saveWorld(array $lastIteration): void
    {
        if (is_array($lastIteration)) {
            $xml = new ParseXml('Xml/world.xml'); // load old world
            $file = $xml->generateXml($lastIteration);
            $file->asXML('Xml/finalWorld.xml'); // save new world
        }
    }

    /**
     * @param mixed[] $world
     * @return array
     */
    private function visualize(array $world): array
    {
        for ($x = 0; $x < $this->dimension; ++$x) {
            for ($y = 0; $y < $this->dimension; ++$y) {
                $world[$x][$y] ? Visualize::fullCell($world[$x][$y]) : Visualize::emptyCell();
            }
            Visualize::breakLine();
        }

        return $world;
    }

    /**
     * @param mixed[] $world
     * @return void
     */
    public function wrapWorld(array $world): void
    {
        Visualize::header();
        $this->visualize($world);
        Visualize::footer();
    }
}